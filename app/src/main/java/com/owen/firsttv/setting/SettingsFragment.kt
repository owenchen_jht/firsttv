package com.owen.firsttv.setting

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.fragment.app.Fragment
import com.owen.firsttv.R
import com.owen.firsttv.databinding.FragmentSettingBinding
import com.owen.firsttv.main.ExchangeFocusListener
import com.owen.firsttv.widget.HeaderView

class SettingsFragment : Fragment(),
    ExchangeFocusListener,
    HeaderView.OnSelectChangeListener,
    View.OnKeyListener,
    ChangeFocusToMenuListener
{

    private lateinit var binding: FragmentSettingBinding

    private var targetItem: View? = null

    private var isLockMenu = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return FragmentSettingBinding.inflate(inflater, container, false).also { binding = it }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.layoutMenuContainer.children.forEach {
            if (it is HeaderView) {
                it.isEnabled = false
                it.addOnSelectChangeListener(this)
                it.setOnKeyListener(this)
            }
        }
    }

    private fun closeFocus() {
        with(binding.layoutMenuContainer) {
            children.filter { it is HeaderView }.forEach {
                it.isEnabled = false
            }
        }
        targetItem?.isSelected = true
    }

    // ====== ExchangeFocusListener ======
    override fun requireFocus() {
        with(binding.layoutMenuContainer) {
            children.filter { it is HeaderView }.forEach {
                it.isEnabled = true
            }

            targetItem?.requestFocus() ?: requestFocus()
        }

        isLockMenu = false
    }

    override fun shouldOpenNav(): Boolean {
        Log.d(TAG, "shouldOpenNav")
        isLockMenu = binding.layoutMenuContainer.children.find { it.isFocused } != null
        if (isLockMenu) {
            closeFocus()
        }
        return isLockMenu
    }
    // ====== ExchangeFocusListener End ======

    // ====== HeaderView.OnSelectChangeListener ======
    override fun onSelectChanged(view: View) {
        Log.d(TAG, "onSelectChanged: $view isFocused: ${view.isFocused}, isSelected: ${view.isSelected}")
        if (isLockMenu) return

        targetItem = view
        val fragment = when (view.id) {
            R.id.app_info -> AppInfoFragment()
            else -> SettingSampleFragment.newInstance(targetItem!!.tag as String, this)
        }

        childFragmentManager.beginTransaction()
            .replace(R.id.contain_layout, fragment)
            .commit()
    }
    // ====== HeaderView.OnSelectChangeListener End ======

    // ====== ChangeFocusToMenuListener  ======
    override fun changeToMenu() {
        requireFocus()
    }
    // ====== ChangeFocusToMenuListener ======


    override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
        if(v?.id == R.id.app_info) return false

        if (v is HeaderView) {
            if (KeyEvent.KEYCODE_DPAD_RIGHT == keyCode && KeyEvent.ACTION_DOWN == event?.action) {
                isLockMenu = binding.layoutMenuContainer.children.find { it.isFocused } != null
                if (isLockMenu) {
                    closeFocus()
                }
            }
        }

        return false
    }


    companion object {
        const val TAG = "SettingsFragment"
    }


}