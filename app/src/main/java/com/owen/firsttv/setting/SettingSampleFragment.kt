package com.owen.firsttv.setting

import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.owen.firsttv.databinding.FragmentSettingSampleBinding

private const val ARG_ITEM_TITLE = "arg_item_title"

class SettingSampleFragment : Fragment() {


    private lateinit var binding: FragmentSettingSampleBinding

    private var title: String? = null

    private lateinit var listener: ChangeFocusToMenuListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            title = it.getString(ARG_ITEM_TITLE)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return FragmentSettingSampleBinding.inflate(inflater).also { binding = it }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.textviewTitle.text = title
        binding.button.setOnClickListener {
            Toast.makeText(requireContext(), "Implement click button", Toast.LENGTH_SHORT).show()
        }
        binding.button.setOnKeyListener { v, keyCode, event ->

            if (KeyEvent.ACTION_DOWN == event.action &&
                KeyEvent.KEYCODE_DPAD_LEFT == keyCode) {
                listener.changeToMenu()
                return@setOnKeyListener true
            }

            return@setOnKeyListener false
        }
    }

    fun setChangeMenuFocusListener(listener: ChangeFocusToMenuListener) {
        this.listener = listener
    }


    companion object {

        @JvmStatic
        fun newInstance(title: String, listener: ChangeFocusToMenuListener) =
            SettingSampleFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_ITEM_TITLE, title)
                }
                setChangeMenuFocusListener(listener)
            }
    }

}