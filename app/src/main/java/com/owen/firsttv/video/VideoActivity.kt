package com.owen.firsttv.video

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.FragmentActivity
import com.johnsonfitness.library.api.atZone.vo.Course
import com.owen.firsttv.R
import com.owen.firsttv.channel.vo.VideoInfo


class VideoActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        when (val source = intent.getParcelableExtra<Parcelable>(EXTRA_SOURCE)) {
            is Course -> {
                showBrightCoveVideo(source)
            }
            is VideoInfo -> {
                showJhtVideo(source)
            }
        }
    }

    private fun showBrightCoveVideo(video : Course) {
        supportFragmentManager.beginTransaction().apply {
            // video fragment
            replace(R.id.videoFragment, BCVideoFragment.newInstance(video), BCVideoFragment.TAG)
        }.commit()
    }

    private fun showJhtVideo(videoInfo: VideoInfo) {
        supportFragmentManager.beginTransaction().apply {
            // video fragment
            replace(R.id.videoFragment, MediaSessionVideoFragment.newInstance(videoInfo), MediaSessionVideoFragment.TAG)
        }.commit()
    }


    companion object {

        private const val EXTRA_SOURCE = "extra_source"

        fun launcher(context: Context, source: Parcelable) {
            context.startActivity(Intent(context, VideoActivity::class.java).apply {
                putExtra(EXTRA_SOURCE, source)
            })
        }
    }
}