package com.owen.firsttv.video

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.brightcove.player.appcompat.BrightcovePlayerFragment
import com.brightcove.player.edge.Catalog
import com.brightcove.player.edge.VideoListener
import com.brightcove.player.event.EventType
import com.brightcove.player.mediacontroller.BrightcoveMediaController
import com.brightcove.player.model.Video
import com.johnsonfitness.library.api.atZone.vo.Course
import com.owen.firsttv.R
import com.owen.firsttv.databinding.FragmentBcVideoBinding


private const val account = "6156693263001"
private const val policy = "BCpkADawqM2RhQZ-VtA5H0DY9ZseD5MlMYYTkUPb8K2n1SUElf5F9oBUtzLY1Vvw_5C4InI2nkfVRkOPE7cA6z_akNSS3WcEaWolDKkF9FazT2TK1MoxxRJCTk9SH_49icKtqS20dA4EuKat"

class BCVideoFragment : BrightcovePlayerFragment() {

    private lateinit var binding: FragmentBcVideoBinding

    private lateinit var mediaController: BrightcoveMediaController


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentBcVideoBinding.inflate(inflater)
        baseVideoView = binding.brightcoveVideoView
        super.onCreateView(inflater, container, savedInstanceState)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG, "onViewCreated: ")
        super.onViewCreated(view, savedInstanceState)

        val videoId = (requireArguments().getParcelable<Course>(ARG_COURSE))!!.videoId!!
        Log.d(TAG, "onViewCreated: videoId: $videoId")

        with(binding.brightcoveVideoView) {
            setMediaController(BrightcoveMediaController(this, R.layout.my_video_controller).also { this@BCVideoFragment.mediaController = it })
            val category = Catalog.Builder(eventEmitter, account)
                .setPolicy(policy)
                .build()

            category.findVideoByID(videoId, object : VideoListener(){
                override fun onVideo(video: Video?) {
                    Log.d(TAG, "onVideo video?.name: ${video?.name}")
                    video ?: return

                    add(video)
                    start()
                }
            })
        }

        mediaController.brightcoveSeekBar.setOnKeyListener { view, keyCode, event ->
            Log.d(TAG, "brightcoveSeekBar OnKeyListener: keyCode: $keyCode mediaController.seekBarOffset: ${mediaController.seekBarOffset}")
            return@setOnKeyListener when (keyCode) {
                KeyEvent.KEYCODE_DPAD_RIGHT -> {
                    if (KeyEvent.ACTION_DOWN == event.action) {
                        binding.brightcoveVideoView.eventEmitter.emit(EventType.FAST_FORWARD)
                    }
                    true
                }
                KeyEvent.KEYCODE_DPAD_LEFT -> {
                    if (KeyEvent.ACTION_DOWN == event.action) {
                        binding.brightcoveVideoView.eventEmitter.emit(EventType.REWIND)
                    }
                    true
                }
                KeyEvent.KEYCODE_DPAD_CENTER,
                KeyEvent.KEYCODE_ENTER,
                KeyEvent.KEYCODE_SPACE,
                KeyEvent.KEYCODE_TV_NUMBER_ENTRY -> {
                    if (KeyEvent.ACTION_DOWN == event.action) {
                        mediaController.brightcoveControlBar.findViewById<Button>(R.id.play).onKeyDown(keyCode, event)
                    }
                    if (KeyEvent.ACTION_UP == event.action) {
                        mediaController.brightcoveControlBar.findViewById<Button>(R.id.play).onKeyUp(keyCode, event)
                    }
                    true
                }
                else -> {
                        false
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop: ")
        binding.brightcoveVideoView.stopPlayback()
    }


    companion object {

        const val TAG = "VideoFragment"

        private const val ARG_COURSE = "arg_courses"

        @JvmStatic
        fun newInstance(course: Course) = BCVideoFragment().apply {
            arguments = Bundle().apply {
                putParcelable(ARG_COURSE, course)
            }
        }
    }
}