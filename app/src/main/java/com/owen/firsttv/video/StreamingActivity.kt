package com.owen.firsttv.video

import android.content.Context
import android.content.Intent
import android.graphics.SurfaceTexture
import android.os.Bundle
import android.util.Log
import android.view.Surface
import android.view.TextureView
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import com.johnsonfitness.library.player.MediaSessionFactory
import com.johnsonfitness.library.player.Source
import com.johnsonfitness.library.player.TYPE
import com.johnsonfitness.library.player.media.AbstractMediaSession
import com.johnsonfitness.library.player.media.OnStateListener
import com.owen.firsttv.databinding.ActivityStreamingBinding
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.resume


class StreamingActivity : AppCompatActivity() {


    private lateinit var binder: ActivityStreamingBinding

    private val tag = "6156693263001"

    private val url = "rtmp://203.133.25.18/pull.johnsonfitness.com.cn/live/test2"

    private var mediaSession: AbstractMediaSession? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(ActivityStreamingBinding.inflate(layoutInflater).also { binder = it }.root)

        binder.start.setOnClickListener {
            mediaSession?.start()
        }
        binder.pause.setOnClickListener {
            mediaSession?.pause()
        }
        binder.stop.setOnClickListener {
            mediaSession?.stop()
        }

        setupVideo()
    }

    override fun onStop() {
        super.onStop()
        mediaSession?.stop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaSession?.release()
    }


    private fun setupVideo() {
        Log.d(TAG, "setupVideo")
        CoroutineScope(Dispatchers.Main).launch {
            mediaSession = MediaSessionFactory.createMediaSession(
                this@StreamingActivity,
                TYPE.EXOPLAYER,
                Source(url, url, 0L)
            ).apply {
                surface = getSurface(binder.frameLayout)
                stateListener = object : OnStateListener {

                    override fun onPreparing(mediaSession: AbstractMediaSession) {
                        Log.d(TAG, "onPreparing: ")
                    }

                    override fun onPrepared(mediaSession: AbstractMediaSession) {
                        Log.d(TAG, "onPrepared: ")
                    }

                    override fun onPaused(mediaSession: AbstractMediaSession) {
                        Log.d(TAG, "onPaused: ")
                    }

                    override fun onStop(mediaSession: AbstractMediaSession) {
                        Log.d(TAG, "onStop: ")
                    }

                    override fun onCompleted(mediaSession: AbstractMediaSession) {
                        Log.d(TAG, "onCompleted: ")
                    }

                    override fun onError(mediaSession: AbstractMediaSession, error: Any?) {
                        Log.d(TAG, "onError: ")
                    }

                }
            }

            mediaSession?.prepare()
        }
    }

    private suspend fun getSurface(parentView: ViewGroup): Surface? {
        return suspendCancellableCoroutine {
            Log.d(TAG, "getSurface suspendCancellableCoroutine: ")
            val textureView = TextureView(this).apply {
                surfaceTextureListener = object : TextureView.SurfaceTextureListener {

                    override fun onSurfaceTextureAvailable(
                        surface: SurfaceTexture,
                        width: Int,
                        height: Int
                    ) {
                        Log.d(TAG, "onSurfaceTextureAvailable: ")
                        if (it.isActive) it.resume(Surface(surface))
                    }

                    override fun onSurfaceTextureSizeChanged(
                        p0: SurfaceTexture,
                        p1: Int,
                        p2: Int
                    ) {
                        Log.d(TAG, "onSurfaceTextureSizeChanged: ")
                    }

                    override fun onSurfaceTextureDestroyed(p0: SurfaceTexture): Boolean = true

                    override fun onSurfaceTextureUpdated(p0: SurfaceTexture) {
                        Log.d(TAG, "onSurfaceTextureUpdated: ")
                    }
                }
            }
            parentView.let {
                it.removeAllViews()
                it.addView(textureView, ViewGroup.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
            }
        }
    }


    companion object {

        private val TAG = StreamingActivity::class.java.simpleName

        fun launcher(context: Context) {
            context.startActivity(Intent(context, StreamingActivity::class.java))
        }
    }
}