package com.owen.firsttv.video

import android.content.Context
import android.content.Intent
import android.graphics.SurfaceTexture
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Surface
import android.view.TextureView
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.FragmentActivity
import com.johnsonfitness.library.player.MediaSessionFactory
import com.johnsonfitness.library.player.Source
import com.johnsonfitness.library.player.TYPE
import com.johnsonfitness.library.player.media.AbstractMediaSession
import com.owen.firsttv.databinding.ActivityMultiVideoBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume

class MultiVideoActivity : FragmentActivity() {

    private lateinit var binder: ActivityMultiVideoBinding

    private val videoMap = ArrayList<AbstractMediaSession>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(ActivityMultiVideoBinding.inflate(layoutInflater).also { binder = it }.root)

        setupVideos()

         binder.textviewVideo1.text = "[exoplayer][${getScheme(urls[0])}][.mpd]"
         binder.textviewVideo2.text = "[exoplayer][${getScheme(urls[1])}][.m3u8]"
         binder.textviewVideo3.text = "[exoplayer][${getScheme(urls[2])}][網宿]"
         binder.textviewVideo4.text = "[exoplayer][${getScheme(urls[3])}][網宿]"
    }

    private fun getScheme(url: String) = Uri.parse(url).scheme


    private fun setupVideos() {
        CoroutineScope(Dispatchers.Main).launch {
            videoMap.add(MediaSessionFactory.createMediaSession(
                this@MultiVideoActivity,
                TYPE.EXOPLAYER,
                Source(urls[0], urls[0], 0L)
            ).apply {
                surface = getSurface(binder.frameLayoutVideo1)
                prepare()
                start()
            })

            videoMap.add(MediaSessionFactory.createMediaSession(
                this@MultiVideoActivity,
                TYPE.EXOPLAYER,
                Source(urls[1], urls[1], 0L)
            ).apply {
                surface = getSurface(binder.frameLayoutVideo2)
                prepare()
                start()
            })

            videoMap.add(MediaSessionFactory.createMediaSession(
                this@MultiVideoActivity,
                TYPE.EXOPLAYER,
                Source(urls[2], urls[2], 0L)
            ).apply {
                surface = getSurface(binder.frameLayoutVideo3)
                prepare()
                start()
            })

            videoMap.add(MediaSessionFactory.createMediaSession(
                this@MultiVideoActivity,
                TYPE.EXOPLAYER,
                Source(urls[3], urls[3], 0L)
            ).apply {
                surface = getSurface(binder.frameLayoutVideo4)
                prepare()
                start()
            })
        }
    }

    private suspend fun getSurface(parentView: ViewGroup): Surface? {
        return suspendCancellableCoroutine {
            Log.d(TAG, "getSurface suspendCancellableCoroutine: ")
            val textureView = TextureView(this).apply {
                surfaceTextureListener = object : TextureView.SurfaceTextureListener {

                    override fun onSurfaceTextureAvailable(
                        surface: SurfaceTexture,
                        width: Int,
                        height: Int
                    ) {
                        if (it.isActive) it.resume(Surface(surface))
                    }

                    override fun onSurfaceTextureSizeChanged(
                        p0: SurfaceTexture,
                        p1: Int,
                        p2: Int
                    ) {
                    }

                    override fun onSurfaceTextureDestroyed(p0: SurfaceTexture): Boolean = true

                    override fun onSurfaceTextureUpdated(p0: SurfaceTexture) {
                    }
                }
            }
            parentView.let {
                it.addView(textureView, 0, ViewGroup.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        videoMap.forEach { it.release() }
    }

    companion object {

        private val urls = arrayOf(
            "https://manifest.prod.boltdns.net/manifest/v1/dash/live-baseurl/clear/5239736509001/2e03c6c6-6aae-4286-a5f1-fe9019376403/6s/manifest.mpd?fastly_token=NjE5NzYyMGFfYjlkZDAxMTI3ZDhmOGVhNjFiMDU2NDg4NTBiNGMxNmJlM2Q4YjI4NjZmZGJkY2FhOTZlOWFmMWQ4ZDQ2NjdiNA%3D%3D&pubid=6156693263001",
            "https://manifest.prod.boltdns.net/manifest/v1/hls/v4/clear/5239736509001/2e03c6c6-6aae-4286-a5f1-fe9019376403/10s/master.m3u8?fastly_token=NjE5NzYyMGFfZDQ0MDIwNDU3MDkxZTdkYzA3MmVhNzU0MWY2ZmZhN2RlNzcwZTExYzFiYzU3OGQ4YWIwOGEwMjFjY2FlNDYyMg%3D%3D&pubid=6156693263001",
            "rtmp://203.133.25.18/pull.johnsonfitness.com.cn/live/test2",
            "rtmp://203.133.25.18/pull.johnsonfitness.com.cn/live/test2"
        )

        private const val TAG =  "MultiVideoActivity"

        fun launcher(context: Context) {
            context.startActivity(Intent(context, MultiVideoActivity::class.java))
        }
    }
}