package com.owen.firsttv.video

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.graphics.SurfaceTexture
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.*
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.johnsonfitness.library.player.MediaSessionsService
import com.owen.firsttv.channel.vo.VideoInfo
import com.owen.firsttv.databinding.FragmentMediaSessionVideoBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume


class MediaSessionVideoFragment : Fragment() {

    private lateinit var binding: FragmentMediaSessionVideoBinding

    private lateinit var videoInfo: VideoInfo

    private var service: MediaSessionsService? = null

    private val connection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName?, _service: IBinder?) {
            service = (_service as MediaSessionsService.MediaSessionsBinder).getService()
            setupPlayer()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            service = null
        }

    }

    private fun setupPlayer() {
        Log.d(TAG, "setupPlayer: ")
        CoroutineScope(Dispatchers.Main).launch {
            service?.getMediaSession(videoInfo.source.videoId)?.let {
                it.setPlayerSurface(getSurface(binding.root)!!)
                it.start()
            }
        }
    }

    private suspend fun getSurface(parentView: ViewGroup): Surface? {
        return suspendCancellableCoroutine {
            Log.d(TAG, "getSurface suspendCancellableCoroutine: ")
            val textureView = TextureView(requireContext()).apply {
                surfaceTextureListener = object : TextureView.SurfaceTextureListener {

                    override fun onSurfaceTextureAvailable(
                        surface: SurfaceTexture,
                        width: Int,
                        height: Int
                    ) {
                        if (it.isActive) it.resume(Surface(surface))
                    }

                    override fun onSurfaceTextureSizeChanged(
                        p0: SurfaceTexture,
                        p1: Int,
                        p2: Int
                    ) {
                    }

                    override fun onSurfaceTextureDestroyed(p0: SurfaceTexture): Boolean = true

                    override fun onSurfaceTextureUpdated(p0: SurfaceTexture) {
                    }
                }
            }
            parentView.let {
                it.addView(textureView, 0, ViewGroup.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        videoInfo = requireArguments().getParcelable(ARG_VIDEO)!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return FragmentMediaSessionVideoBinding.inflate(inflater).also { binding = it }.root
    }

    override fun onStart() {
        super.onStart()
        requireContext().bindService(MediaSessionsService.getIntent(requireContext()), connection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        service?.getMediaSession(videoInfo.source.videoId)?.pause()
        requireContext().unbindService(connection)
    }


    companion object {

        const val TAG = "MediaSessionVideoFragment"

        private const val ARG_VIDEO = "ARG_VIDEO_INFO"

        @JvmStatic
        fun newInstance(videoInfo: VideoInfo) =
            MediaSessionVideoFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_VIDEO, videoInfo)
                }
            }
    }
}