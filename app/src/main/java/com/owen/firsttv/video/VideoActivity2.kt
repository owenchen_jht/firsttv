package com.owen.firsttv.video

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.SurfaceTexture
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.Surface
import android.view.TextureView
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.FragmentActivity
import com.johnsonfitness.library.player.MediaSessionsService
import com.johnsonfitness.library.player.MediaSessionFactory
import com.johnsonfitness.library.player.Source
import com.johnsonfitness.library.player.TYPE
import com.owen.firsttv.databinding.ActivityVideo2Binding
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.resume


class VideoActivity2 : FragmentActivity(), ServiceConnection {


    private lateinit var binder: ActivityVideo2Binding

    private val tag = "6156693263001"

    private val videoId = "rtmp:test2"

    private val url = "http://manifest.prod.boltdns.net/manifest/v1/dash/live-baseurl/clear/5239736509001/2e03c6c6-6aae-4286-a5f1-fe9019376403/6s/manifest.mpd?fastly_token=NjE5NzYyMGFfYjlkZDAxMTI3ZDhmOGVhNjFiMDU2NDg4NTBiNGMxNmJlM2Q4YjI4NjZmZGJkY2FhOTZlOWFmMWQ4ZDQ2NjdiNA%3D%3D&pubid=6156693263001"

    private lateinit var videoService: MediaSessionsService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(ActivityVideo2Binding.inflate(layoutInflater).also { binder = it }.root)

        binder.start.setOnClickListener {
            videoService.getMediaSession(videoId)?.start()
        }
        binder.pause.setOnClickListener {
            videoService.getMediaSession(videoId)?.pause()
        }
        binder.stop.setOnClickListener {
            videoService.getMediaSession(videoId)?.stop()
        }

        bindService(MediaSessionsService.getIntent(this), this, Context.BIND_AUTO_CREATE)
    }


    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        videoService.getMediaSession(videoId)?.release()

        unbindService(this)
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        if (service is MediaSessionsService.MediaSessionsBinder) {
            CoroutineScope(Dispatchers.Main).launch {
                videoService = service.getService().apply {
                    addMediaSession(
                        MediaSessionFactory.createMediaSession(
                            this@VideoActivity2,
                            TYPE.EXOPLAYER,
                            Source(videoId, url, 0L)
                        ).apply {
                            surface = getSurface(binder.frameLayout)
                            prepare()
                        },
                        object : MediaSessionsService.OnMediaStateListener {
                            override fun onPreparing(videoId: String) {
                                Log.d(TAG, "onPreparing: ")
                            }

                            override fun onPrepared(videoId: String) {
                                Log.d(TAG, "onPrepared: ")
                            }

                            override fun onPaused(videoId: String) {
                                Log.d(TAG, "onPaused: ")
                            }

                            override fun onStop(videoId: String) {
                                Log.d(TAG, "onStop: ")
                            }

                            override fun onCompleted(videoId: String) {
                                Log.d(TAG, "onCompleted: ")
                            }

                            override fun onError(videoId: String) {
                                Log.d(TAG, "onError: ")
                            }

                        })
                }
            }
        }
    }

    override fun onServiceDisconnected(name: ComponentName?) {
    }


    private suspend fun getSurface(parentView: ViewGroup): Surface? {
        return suspendCancellableCoroutine {
            Log.d(TAG, "getSurface suspendCancellableCoroutine: ")
            val textureView = TextureView(this).apply {
                surfaceTextureListener = object : TextureView.SurfaceTextureListener {

                    override fun onSurfaceTextureAvailable(
                        surface: SurfaceTexture,
                        width: Int,
                        height: Int
                    ) {
                        Log.d(TAG, "onSurfaceTextureAvailable: ")
                        if (it.isActive) it.resume(Surface(surface))
                    }

                    override fun onSurfaceTextureSizeChanged(
                        p0: SurfaceTexture,
                        p1: Int,
                        p2: Int
                    ) {
                        Log.d(TAG, "onSurfaceTextureSizeChanged: ")
                    }

                    override fun onSurfaceTextureDestroyed(p0: SurfaceTexture): Boolean = true

                    override fun onSurfaceTextureUpdated(p0: SurfaceTexture) {
                        Log.d(TAG, "onSurfaceTextureUpdated: ")
                    }
                }
            }
            parentView.let {
                it.removeAllViews()
                it.addView(textureView, ViewGroup.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT))
            }
        }
    }


    companion object {

        private val TAG = VideoActivity2::class.java.simpleName

        fun launcher(context: Context) {
            context.startActivity(Intent(context, VideoActivity2::class.java))
        }
    }
}