package com.owen.firsttv.feature

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.owen.firsttv.R


class DialogActivity : FragmentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dialog)

        supportFragmentManager.beginTransaction()
            .replace(R.id.container, DialogFragment())
            .commit()
    }

    companion object {

        fun launcher(context: Context) {
            context.startActivity(Intent(context, DialogActivity::class.java))
        }
    }
}