package com.owen.firsttv.feature

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.owen.firsttv.databinding.FragmentFeatureBinding
import com.owen.firsttv.main.ExchangeFocusListener


class FeatureFragment : Fragment(), ExchangeFocusListener {

    private lateinit var binding: FragmentFeatureBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return FragmentFeatureBinding.inflate(inflater).also { binding = it }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            dialog.setOnClickListener {
                AlertDialog.Builder(requireContext())
                    .setMessage("test dialog")
                    .setPositiveButton("ok") { dialog, _ -> dialog.dismiss() }
                    .create()
                    .show()
            }

            dialogTv.setOnClickListener {
                DialogActivity.launcher(requireContext())
            }

            guideline.setOnClickListener {
                OnboardingActivity.launcher(requireContext())
            }
        }
    }

    override fun requireFocus() {
        binding.root.requestFocus()
    }

    override fun shouldOpenNav(): Boolean {
        return binding.dialog.isFocused or binding.guideline.isFocused
    }

    companion object {

        @JvmStatic
        fun newInstance() = FeatureFragment()
    }

}