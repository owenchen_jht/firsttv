package com.owen.firsttv.feature

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.leanback.app.OnboardingSupportFragment
import com.owen.firsttv.R


class OnboardingFragment : OnboardingSupportFragment() {


    private val pageTitles = arrayListOf(
        "Page 1",
        "Page 2",
        "Page 3",
        "Page 4"
    )

    private val pageDescriptions = arrayListOf(
        "Used OnboardingSupportFragment\nDescriptions two",
        "Descriptions two",
        "Descriptions three",
        "Descriptions four"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        logoResourceId = R.drawable.app_icon_your_company
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    override fun getPageCount(): Int = pageTitles.size

    override fun getPageTitle(pageIndex: Int): CharSequence {
        return pageTitles[pageIndex]
    }

    override fun getPageDescription(pageIndex: Int): CharSequence {
        return pageDescriptions[pageIndex]
    }

    override fun onCreateBackgroundView(inflater: LayoutInflater?, container: ViewGroup?): View? {
        return inflater?.inflate(R.layout.view_onboard_background, container, false)
    }

    override fun onCreateContentView(inflater: LayoutInflater?, container: ViewGroup?): View? {
        return inflater?.inflate(R.layout.view_onboard_content, container, false)
    }

    override fun onCreateForegroundView(inflater: LayoutInflater?, container: ViewGroup?): View? {
        return inflater?.inflate(R.layout.view_onboard_foreground, container, false)
    }

    override fun onFinishFragment() {
        super.onFinishFragment()
        //TODO complete onboarding, flag set false
    }

    companion object {

    }
}