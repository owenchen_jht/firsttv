package com.owen.firsttv.feature

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import com.owen.firsttv.R


class OnboardingActivity : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)
    }

    companion object {

        fun launcher(context: Context) {
            context.startActivity(Intent(context, OnboardingActivity::class.java))
        }
    }
}