package com.owen.firsttv.feature


import android.os.Bundle
import androidx.leanback.app.GuidedStepSupportFragment
import androidx.leanback.widget.GuidanceStylist
import androidx.leanback.widget.GuidanceStylist.Guidance
import androidx.leanback.widget.GuidedAction
import com.owen.firsttv.R


class DialogFragment : GuidedStepSupportFragment() {

    override fun onCreateGuidance(savedInstanceState: Bundle?): GuidanceStylist.Guidance {
        return Guidance("title", "description", "step: {breadcrumb}", null)
    }

    override fun onCreateActions(actions: MutableList<GuidedAction>, savedInstanceState: Bundle?) {
        actions.add(GuidedAction.Builder(requireContext())
            .id(R.id.actions_ok.toLong())
            .title("action title OK")
            .description("action description")
            .icon(R.drawable.all_home_24)
            .build())

        actions.add(GuidedAction.Builder(requireContext())
            .id(R.id.actions_cancel.toLong())
            .title("action title cancel")
            .build())
    }
}