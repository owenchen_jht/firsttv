package com.owen.firsttv.channel

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.leanback.widget.ImageCardView
import androidx.leanback.widget.Presenter
import com.bumptech.glide.Glide
import com.johnsonfitness.library.api.atZone.vo.Course
import com.owen.firsttv.R

class CoursePresenter() : Presenter() {

    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder = CourseViewHolder(parent)

    override fun onBindViewHolder(viewHolder: ViewHolder, item: Any?) {
        (viewHolder as CourseViewHolder).bind(item as Course)
    }

    override fun onUnbindViewHolder(viewHolder: ViewHolder?) {

        (viewHolder?.view as? ImageCardView)?.let {
            it.badgeImage = null
            it.mainImage = null
        }
    }

    private class CourseViewHolder(parent: ViewGroup) : Presenter.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_course, parent, false)
    ) {

        private val cardView: ImageCardView =  (view.findViewById(R.id.image_card_view) as ImageCardView)

        init {
//            sDefaultBackgroundColor = ContextCompat.getColor(parent.context, R.color.default_background)
//            sSelectedBackgroundColor = ContextCompat.getColor(parent.context, R.color.selected_background)
//
//            cardView = (view.findViewById(R.id.image_card_view) as ImageCardView).apply {
//
//            }

            with(cardView) {
                setMainImageDimensions(context.resources.getDimension(R.dimen.card_course_width).toInt(), context.resources.getDimension(R.dimen.card_course_height).toInt())
            }
        }

        fun bind(course: Course) {

            with(cardView) {
                titleText = course.title
                contentText = course.description

                Glide.with(context)
                    .load(course.snapshotUrl)
                    .centerCrop()
                    .error(R.drawable.movie)
                    .into(cardView.mainImageView)
            }
        }

//        companion object {
//
//            private var sDefaultBackgroundColor: Int by Delegates.notNull()
//            private var sSelectedBackgroundColor: Int by Delegates.notNull()
//        }
    }
}