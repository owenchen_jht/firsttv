package com.owen.firsttv.channel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.johnsonfitness.library.api.atZone.vo.Course
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class ChannelViewModel: ViewModel() {

    private val repository by lazy { ChannelRepository.instance }

    private val job = Job()

    private val scope = CoroutineScope(Dispatchers.IO + job)

    private val spinner = MutableLiveData<Boolean>()

    fun getSpinner() = spinner as LiveData<Boolean>

    val courses = MutableLiveData<List<Course>>()


    fun getCourses() {
        scope {
            repository.getCourses()?.let {
                courses.postValue(it.data.paginate.data)
            }
        }

        spinner.postValue(null)
    }


    private fun scope(block: suspend () -> Unit ): Job {
        return scope.launch {
            try {
                spinner.postValue(true)
                block()
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                spinner.postValue(false)
            }
        }
    }
}