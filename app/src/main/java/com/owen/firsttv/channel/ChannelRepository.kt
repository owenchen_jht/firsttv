package com.owen.firsttv.channel

import com.johnsonfitness.library.api.ApiManager


class ChannelRepository {

    private val apiManager: ApiManager by lazy { ApiManager.instance }

    suspend fun getCourses() = apiManager.getCourses(0)

    companion object {
        val instance by lazy { ChannelRepository() }
    }
}