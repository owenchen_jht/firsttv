package com.owen.firsttv.channel

import android.graphics.SurfaceTexture
import android.util.Log
import android.view.*
import androidx.leanback.widget.Presenter
import com.bumptech.glide.Glide
import com.johnsonfitness.library.player.MediaSessionsService
import com.johnsonfitness.library.player.MediaSessionFactory
import com.johnsonfitness.library.player.TYPE
import com.johnsonfitness.library.player.media.AbstractMediaSession
import com.johnsonfitness.library.player.media.IjkPlayerMediaSession
import com.owen.firsttv.R
import com.owen.firsttv.channel.vo.VideoInfo
import com.owen.firsttv.databinding.ItemVideoBinding


class VideoPresenter(private val service: MediaSessionsService?) : Presenter() {


    override fun onCreateViewHolder(parent: ViewGroup): ViewHolder {
        return VideoVideHolder(
            ItemVideoBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder?, item: Any?) {
        (viewHolder as VideoVideHolder).bind(item as VideoInfo)
    }

    override fun onUnbindViewHolder(viewHolder: ViewHolder?) {
        // Remove media session
        (viewHolder as VideoVideHolder).unbind()
    }


    inner class VideoVideHolder(private val binding: ItemVideoBinding) :
        Presenter.ViewHolder(binding.root),
        MediaSessionsService.OnMediaStateListener {

        private var mediaSession: AbstractMediaSession? = null

        init {
            view.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
                Log.d(
                    "VideoVideHolder",
                    "OnFocusChangeListener hasFocus: $hasFocus mediaSession: $mediaSession"
                )
                if (hasFocus) {
                    when (mediaSession) {
                        is IjkPlayerMediaSession -> {
                            with(mediaSession as IjkPlayerMediaSession) {
                                if (!this.isPrepared) {
                                    prepare()
                                }
                                start()
                            }
                        }
                        else -> {
                            mediaSession?.prepare()
                            mediaSession?.start()
                        }
                    }
                } else {
                    mediaSession?.stop()
                    mediaSession?.prepare()
                }
            }

            binding.textureView.surfaceTextureListener =
                object : TextureView.SurfaceTextureListener {
                    override fun onSurfaceTextureAvailable(
                        surface: SurfaceTexture,
                        width: Int,
                        height: Int
                    ) {
                        Log.d(
                            "VideoVideHolder",
                            "onSurfaceTextureAvailable: mediaSession: $mediaSession"
                        )
                        mediaSession?.setPlayerSurface(Surface(surface))
                    }

                    override fun onSurfaceTextureSizeChanged(
                        surface: SurfaceTexture,
                        width: Int,
                        height: Int
                    ) {
                    }

                    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
                        Log.d("VideoVideHolder", "onSurfaceTextureDestroyed")
                        return true
                    }

                    override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {
                        Log.d("VideoVideHolder", "onSurfaceTextureUpdated: $surface")
                        if (view.isFocused) {
                            mediaSession?.setPlayerSurface(Surface(surface))
                        }
                    }
                }
        }

        fun bind(videoInfo: VideoInfo) {
            Log.d("VideoVideHolder", "bind: service: $service")
            // add media session to service
            service?.addMediaSession(
                MediaSessionFactory.createMediaSession(
                    view.context,
                    TYPE.EXOPLAYER,
                    videoInfo.source
                ).also { mediaSession = it },
                this
            )
            binding.title.text = videoInfo.videoTitle

            Glide.with(view.context)
                .load(videoInfo.videoImagePath)
                .centerCrop()
                .error(R.drawable.movie)
                .into(binding.imageview)

            Log.d("VideoVideHolder", "bind: mediaSession: $mediaSession")
        }

        fun unbind() {
            Log.d("VideoVideHolder", "unbind: ")
            mediaSession?.surface?.release()
            mediaSession?.getVideoId()?.let { service?.removeMediaSession(it) }
        }

        override fun onPreparing(videoId: String) {
            Log.d("VideoVideHolder", "onPreparing: ")
        }

        override fun onPrepared(videoId: String) {
            Log.d("VideoVideHolder", "onPrepared: ")
            binding.imageview.visibility = View.GONE
        }

        override fun onPaused(videoId: String) {
        }

        override fun onStop(videoId: String) {
            Log.d("VideoVideHolder", "onStop: ")
            binding.imageview.visibility = View.VISIBLE
        }

        override fun onCompleted(videoId: String) {
            binding.imageview.visibility = View.VISIBLE
        }

        override fun onError(videoId: String) {
        }
    }
}