package com.owen.firsttv.channel

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.leanback.app.VerticalGridSupportFragment
import androidx.leanback.widget.ArrayObjectAdapter
import androidx.leanback.widget.FocusHighlight
import androidx.leanback.widget.VerticalGridPresenter
import androidx.lifecycle.ViewModelProvider
import com.johnsonfitness.library.api.atZone.vo.Course
import com.owen.firsttv.main.ExchangeFocusListener
import com.owen.firsttv.video.VideoActivity


private const val ARG_CHANNEL_ID = "arg_channel_id"


class VideoGridFragment : VerticalGridSupportFragment(), ExchangeFocusListener {


    private val columns = 5

    private var channelId: String? = null

    private var mRowsAdapter: ArrayObjectAdapter? = null

    private lateinit var viewModel: ChannelViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            channelId = it.getString(ARG_CHANNEL_ID)
        }

        gridPresenter = VerticalGridPresenter(FocusHighlight.ZOOM_FACTOR_MEDIUM).apply {
            numberOfColumns = columns
        }

        adapter = ArrayObjectAdapter(CoursePresenter()).also { mRowsAdapter = it }

        setOnItemViewSelectedListener { _, item, _, _ ->
            indexOfItem = (adapter as ArrayObjectAdapter).indexOf(item)
        }

        setOnItemViewClickedListener { _, item, _, _ ->
            VideoActivity.launcher(requireContext(), item as Course)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ChannelViewModel::class.java).apply {

            courses.observe(viewLifecycleOwner) { list ->
                title = "Courses list";
                mRowsAdapter?.addAll(adapter.size(), list)
            }
        }

        Log.d(TAG, "onViewCreated: $channelId")
        if (channelId == "0") {
            viewModel.getCourses()
        }
    }


    //====== ExchangeFocusListener =======
    override fun requireFocus() {
    }

    override fun shouldOpenNav(): Boolean {
        Log.d(TAG, "shouldOpenNav indexOfItem: $indexOfItem ")
        return indexOfItem % columns == 0
    }
    //====== ExchangeFocusListener End =======


    companion object {

        private const val TAG = "VideoGridFragment"

        private var indexOfItem = -1


        @JvmStatic
        fun newInstance(channelId: String) = VideoGridFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_CHANNEL_ID, channelId)
            }
        }
    }
}