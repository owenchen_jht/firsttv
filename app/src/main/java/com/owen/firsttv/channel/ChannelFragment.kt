package com.owen.firsttv.channel

import android.content.ComponentName
import android.content.Context
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.os.Parcelable
import android.util.Log
import android.view.View
import androidx.leanback.app.RowsSupportFragment
import androidx.leanback.widget.*
import androidx.lifecycle.ViewModelProvider
import com.johnsonfitness.library.api.atZone.vo.Course
import com.johnsonfitness.library.player.MediaSessionsService
import com.johnsonfitness.library.player.Source
import com.owen.firsttv.channel.vo.VideoInfo
import com.owen.firsttv.main.ExchangeFocusListener
import com.owen.firsttv.video.MultiVideoActivity
import com.owen.firsttv.video.StreamingActivity
import com.owen.firsttv.video.VideoActivity
import com.owen.firsttv.video.VideoActivity2


private const val ARG_CHANNEL_ID = "arg_channel_id"


class ChannelFragment : RowsSupportFragment(), ExchangeFocusListener {


    private var channelId: String? = null

    private var mRowsAdapter: ArrayObjectAdapter? = null

    private lateinit var viewModel: ChannelViewModel

    private var service: MediaSessionsService? = null

    private var connection = object : ServiceConnection {

        override fun onServiceConnected(name: ComponentName?, service: IBinder) {
            Log.d(TAG, "onServiceConnected: ")
            this@ChannelFragment.service = (service as MediaSessionsService.MediaSessionsBinder).getService()
            Log.d(TAG, "onServiceConnected: service: $service")
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.d(TAG, "onServiceConnected: ")
            this@ChannelFragment.service = null
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { channelId = it.getString(ARG_CHANNEL_ID) }

        adapter = ArrayObjectAdapter(ListRowPresenter()).also { mRowsAdapter = it }

        onItemViewSelectedListener = BaseOnItemViewSelectedListener<ListRow?> { _, item, _, row ->
            indexOfItem = ((row as ListRow).adapter as ArrayObjectAdapter).indexOf(item)
        }

        onItemViewClickedListener = BaseOnItemViewClickedListener<ListRow> {
                itemViewHolder, item, rowViewHolder, row ->

            if (item is Course) {
                if (item.id == "-1") {
                    MultiVideoActivity.launcher(requireContext())
                    return@BaseOnItemViewClickedListener
                }

                if (item.id == "-2") {
                    StreamingActivity.launcher(requireContext())
                    return@BaseOnItemViewClickedListener
                }

                if (item.id == "-3") {
                    VideoActivity2.launcher(requireContext())
                    return@BaseOnItemViewClickedListener
                }
            }

            VideoActivity.launcher(requireContext(), item as Parcelable)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(this).get(ChannelViewModel::class.java).apply {

            courses.observe(viewLifecycleOwner) { list ->

                initVideoData()

                initTestData()

                initData("Courses", list)
            }
        }

        if (channelId == "1") {
            viewModel.getCourses()
        }
    }

    private fun initVideoData() {
        val list = ArrayList<VideoInfo>().apply {
            add(VideoInfo(
                videoTitle = "210907-Yaya-Strength-Live-Mix-02",
                videoImagePath = "https://cf-images.us-east-1.prod.boltdns.net/v1/jit/6156693263001/99a82f04-ae4e-4849-bc0c-e7666461d13b/main/160x90/10m224ms/match/image.jpg",
                source = Source(
                    "6283324101001",
                    "https://manifest.prod.boltdns.net/manifest/v1/hls/v4/clear/6156693263001/99a82f04-ae4e-4849-bc0c-e7666461d13b/10s/master.m3u8?fastly_token=NjE5ZjQ2ZjRfZDA0OTc5NTU0MmE3MjFhMjg3NjVmMmNjY2Q3ZTk4ZTljMjgwODRkNzQwYmE1NjBlY2M4NzQyNGEwZDcxYTMyYg%3D%3D",
                    0L
                )
            ))

            add(VideoInfo(
                videoTitle = "Yaya-Strength-Live-Mix-01",
                videoImagePath = "https://cf-images.us-east-1.prod.boltdns.net/v1/jit/6156693263001/f0614db7-adf3-4840-96b9-0e8984754ff3/main/160x90/10m725ms/match/image.jpg",
                source = Source(
                    "6283320971001",
                    "https://manifest.prod.boltdns.net/manifest/v1/hls/v4/clear/6156693263001/99a82f04-ae4e-4849-bc0c-e7666461d13b/10s/master.m3u8?fastly_token=NjE5ZTRjN2NfYzNhMjEyNTg0MDUwZjFmZWI4MTA4MTkxMjk0ZTQzNGRhNDI1NmI4OTEyNjk5YjllYmIyYzgwNDIwNjVmYTM0Nw%3D%3D",
                    0L
                )
            ))
        }

        val adapter = ArrayObjectAdapter(VideoPresenter(service)).apply {
            addAll(0, list)
        }
        mRowsAdapter?.add(ListRow(HeaderItem("VideoPresenter"), adapter))
    }

    private fun initTestData() {
        val list = ArrayList<Course>().apply {
            add(Course(
                id = "-1",
                title = "multi-video"
            ))
            add(Course(
                id = "-2",
                title = "rtmp video"
            ))
            add(Course(
                id = "-3",
                title = "video in service"
            ))
        }

        initData("Test", list)
    }

    private fun initData(title: String, movieList: List<Course>) {
        val adapter = ArrayObjectAdapter(CoursePresenter())
        movieList.forEach {
            adapter.add(it)
        }

        mRowsAdapter?.add(ListRow(HeaderItem(title), adapter))
    }

    override fun onStart() {
        super.onStart()
        requireContext().bindService(MediaSessionsService.getIntent(requireContext()), connection,  Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        requireContext().unbindService(connection)
    }

    //====== ExchangeFocusListener =======
    override fun requireFocus() {
    }

    override fun shouldOpenNav(): Boolean {
        Log.d(TAG, "shouldOpenNav indexOfItem: $indexOfItem ")
        return indexOfItem == 0
    }
    //====== ExchangeFocusListener End =======


    companion object {

        private val TAG = "ChannelFragment"

        private var indexOfItem = -1

        @JvmStatic
        fun newInstance(channelId: String) =
            ChannelFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_CHANNEL_ID, channelId)
                }
            }
    }
}