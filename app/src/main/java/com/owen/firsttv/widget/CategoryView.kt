package com.owen.firsttv.widget

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import com.owen.firsttv.R
import com.owen.firsttv.databinding.ViewCategoryBinding
import com.owen.firsttv.databinding.ViewHeaderBinding


class CategoryView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
): FrameLayout(context, attrs, defStyleAttr) {

    private var binding: ViewCategoryBinding = ViewCategoryBinding.inflate(LayoutInflater.from(context), this, true)

    private var selectChangeListener: OnSelectChangeListener? = null

    init {
        attrs?.let {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.CategoryView)
            try {
                setBackgroundResource(ta.getResourceId(R.styleable.CategoryView_android_background, 0))
                with(binding.textviewText) {
                    text = ta.getString(R.styleable.CategoryView_android_text)
                    setTextColor(ta.getColorStateList(R.styleable.CategoryView_android_textColor))
                }

                binding.viewDivider.setBackgroundResource(ta.getResourceId(R.styleable.CategoryView_divider_color, 0))
            } finally {
                ta.recycle()
            }
        }

        setOnFocusChangeListener { view, isFocused ->
            //for change state
            isSelected = isFocused
            if (isSelected) {
                selectChangeListener?.onSelectChanged(view)
            }
        }
    }

    fun addOnSelectChangeListener(listener: OnSelectChangeListener) {
        selectChangeListener = listener
    }


    interface OnSelectChangeListener {
        fun onSelectChanged(view: View)
    }


    companion object {
        private const val TAG = "CategoryView"
    }
}
