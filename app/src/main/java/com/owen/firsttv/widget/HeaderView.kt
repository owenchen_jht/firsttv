package com.owen.firsttv.widget

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.owen.firsttv.R
import com.owen.firsttv.databinding.ViewHeaderBinding


class HeaderView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
): FrameLayout(context, attrs, defStyleAttr) {

    private var binding: ViewHeaderBinding = ViewHeaderBinding.inflate(LayoutInflater.from(context), this, true)

    private var selectChangeListener: OnSelectChangeListener? = null

    init {
        attrs?.let {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.HeaderView)
            try {
                setBackgroundResource(ta.getResourceId(R.styleable.HeaderView_android_background, 0))
                with(binding.textviewText) {
                    text = ta.getString(R.styleable.HeaderView_android_text)
                    setTextColor(ta.getColorStateList(R.styleable.HeaderView_android_textColor))
                }

                val imageId = ta.getResourceId(R.styleable.HeaderView_icon, -1)
                if (imageId > 0) {
                    with(binding.imageviewIcon){
                        setImageResource(imageId)
                        imageTintList = ta.getColorStateList(R.styleable.HeaderView_tint)
                        visibility = View.VISIBLE
                    }
                }

            } finally {
                ta.recycle()
            }
        }

        setOnFocusChangeListener { view, isFocused ->
            //for change state
            Log.d(TAG, "binding.textviewText: isActivated ${binding.textviewText.isActivated}")
            isSelected = isFocused
            if (isSelected) {
                selectChangeListener?.onSelectChanged(view)
            }
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        binding.textviewText.isEnabled = enabled
    }

    fun addOnSelectChangeListener(listener: OnSelectChangeListener) {
        selectChangeListener = listener
    }

    fun close() {
        binding.textviewText.alpha = 0f
    }

    fun open() {
        binding.textviewText.alpha = 1f
    }


    interface OnSelectChangeListener {
        fun onSelectChanged(view: View)
    }


    companion object {
        private const val TAG = "HeaderView"
    }
}
