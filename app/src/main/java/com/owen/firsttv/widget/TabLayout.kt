package com.owen.firsttv.widget

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.View.OnFocusChangeListener
import android.widget.LinearLayout
import androidx.leanback.tab.LeanbackTabLayout
import androidx.viewpager.widget.ViewPager

class TabLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet, defStyleAttr: Int = 0
) : LeanbackTabLayout(context, attrs, defStyleAttr) {

    private var viewPager: ViewPager? = null

    override fun setupWithViewPager(viewPager: ViewPager?) {
        super.setupWithViewPager(viewPager)
        this.viewPager = viewPager
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        val tabStrip = getChildAt(0) as LinearLayout ?: return

        val tabCount = tabStrip.childCount
        for (i in 0 until tabCount) {
            val tabView = tabStrip.getChildAt(i)
            tabView.isFocusable = true
            tabView.onFocusChangeListener = object : TabFocusChangeListener(this, viewPager){}
        }
    }
}


internal open class TabFocusChangeListener(
    var mLeanbackTabLayout: LeanbackTabLayout,
    var mViewPager: ViewPager?
) :
    OnFocusChangeListener {
    override fun onFocusChange(v: View, hasFocus: Boolean) {
        if (hasFocus) {
            val tabStrip = mLeanbackTabLayout.getChildAt(0) as LinearLayout
            for (i in 0 until tabStrip.childCount) {
                if (v === tabStrip.getChildAt(i)) {
                    if (mViewPager != null) {
                        mViewPager!!.setCurrentItem(i, false)
                    }
                }
            }
        }
    }
}