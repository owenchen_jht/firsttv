package com.owen.firsttv.main

interface ExchangeFocusListener {

    fun requireFocus()

    fun shouldOpenNav() : Boolean
}