package com.owen.firsttv.main

import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.johnsonfitness.library.player.MediaSessionsService
import com.owen.firsttv.R
import com.owen.firsttv.databinding.ActivityMainBinding
import com.owen.firsttv.feature.FeatureFragment
import com.owen.firsttv.search.SearchActivity
import com.owen.firsttv.setting.SettingsFragment

class MainActivity : FragmentActivity(), NavigationAction {

    private lateinit var binding: ActivityMainBinding

    private lateinit var navFragment: NavFragment

    private lateinit var viewModel: MainViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        navFragment = supportFragmentManager.findFragmentByTag(NavFragment.TAG) as NavFragment

        fragmentReplacer(R.id.layout_container, HomeFragment())

        viewModel = ViewModelProvider(this)[MainViewModel::class.java].apply {
            selectedType.value = getString(R.string.navigate_home)

            selectedType.observe(this@MainActivity) { tag ->
                val fragment = when(tag) {
                    getString(R.string.navigate_home) -> HomeFragment()
                    getString(R.string.navigate_setting) -> SettingsFragment()
                    getString(R.string.navigate_feature) -> FeatureFragment()
                    else  -> null
                }

                fragment?.let { fragmentReplacer(R.id.layout_container, it) }
            }
        }

        MediaSessionsService.startService(this.applicationContext)
    }

    private fun fragmentReplacer(containerId: Int, fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(containerId, fragment).commit()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        Log.d(TAG, "onKeyDown: keyCode: $keyCode")
        when (keyCode) {
            KeyEvent.KEYCODE_DPAD_LEFT -> {
                if (findVisibleFragment().shouldOpenNav() && !navFragment.isOpen) {
                    navFragment.openNav(true)
                    return true
                }
            }
        }

        return super.onKeyDown(keyCode, event)
    }

    override fun onStateChanged(expanded: Boolean) {
        if (!expanded) {
            findVisibleFragment().requireFocus()
        }
    }

    override fun onClicked(tag: String?) {
        if (getString(R.string.navigate_search) == tag)  {
            SearchActivity.launch(this)
        }
    }

    private fun findVisibleFragment() : ExchangeFocusListener {
        return supportFragmentManager.fragments
            .filter { it.isVisible && it is ExchangeFocusListener}[0] as ExchangeFocusListener
    }


    companion object {
        const val TAG = "MainActivity"
    }

}