package com.owen.firsttv.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.owen.firsttv.channel.ChannelFragment
import com.owen.firsttv.channel.VideoGridFragment
import com.owen.firsttv.databinding.FragmentHomeBinding


class HomeFragment : Fragment(), ExchangeFocusListener {


    private lateinit var homeBinding : FragmentHomeBinding

    private lateinit var actionListener: NavigationAction

    private lateinit var adapter: TopTabViewPagerAdapter

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is NavigationAction) {
            actionListener = context
        } else {
            throw IllegalArgumentException("$context must be implement NavigationAction.")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        homeBinding = FragmentHomeBinding.inflate(inflater, container, false)
        return homeBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with (homeBinding) {
            tabLayout.setupWithViewPager(viewPager)
            viewPager.adapter =
                TopTabViewPagerAdapter(childFragmentManager, CHANNELS.toList()).also {
                    adapter = it
                }
        }
    }


    //====== ExchangeFocusListener ======
    override fun requireFocus() {
        homeBinding.root.requestFocus()
    }

    /** return is focused on tab 0 or left item on page */
    override fun shouldOpenNav(): Boolean {
        return (homeBinding.tabLayout.selectedTabPosition == 0
                && (homeBinding.tabLayout.getChildAt(0) as ViewGroup).getChildAt(0).isFocused)
                || (adapter.getItem(homeBinding.viewPager.currentItem) as ExchangeFocusListener).shouldOpenNav()
    }
    //====== ExchangeFocusListener End ======


    private inner class TopTabViewPagerAdapter(fragmentManager: FragmentManager, val channels: List<String>) : FragmentStatePagerAdapter(fragmentManager) {

        override fun getCount() = channels.size

        override fun getItem(position: Int): Fragment {
            //TODO Temporarily use the position value as the channel ID
            return when (position) {
                0 -> VideoGridFragment.newInstance(position.toString())
                else -> ChannelFragment.newInstance(position.toString())
            }
        }

        override fun getPageTitle(position: Int): CharSequence {
            return channels[position]
        }
    }


    companion object {
        val CHANNELS = arrayListOf("Channel 0", "Channel 1", "Channel 2", "Channel 3")

        const val TAG = "HomeFragment"
    }
}


