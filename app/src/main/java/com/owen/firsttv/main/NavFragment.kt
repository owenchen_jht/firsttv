package com.owen.firsttv.main

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.owen.firsttv.R
import com.owen.firsttv.databinding.FragmentNavBinding
import com.owen.firsttv.widget.HeaderView
import kotlin.math.max
import kotlin.math.min

class NavFragment : Fragment(), View.OnKeyListener, HeaderView.OnSelectChangeListener {


    private lateinit var binding: FragmentNavBinding

    private lateinit var viewModel: MainViewModel

    private lateinit var actionListener: NavigationAction

    private var minWidth = 0

    private var maxWidth = 0

    private var internalOffset = 0

    var isOpen = true

    private val minWidthRunnable = object : Runnable {
        override fun run() {
            binding.root.let {
                if (it.measuredWidth > minWidth) {
                    it.layoutParams.width = max(minWidth, it.measuredWidth - internalOffset)
                    it.requestLayout()
                    it.handler.postDelayed(this, 10)
                }
            }
        }
    }

    private val maxWidthRunnable = object : Runnable {
        override fun run() {
            binding.root.let {
                if (it.measuredWidth < maxWidth) {
                    it.layoutParams.width = min(maxWidth, it.measuredWidth + internalOffset)
                    it.requestLayout()
                    it.handler.postDelayed(this, 10)
                }
            }
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is NavigationAction) {
            actionListener = context
        } else {
            throw IllegalArgumentException("$context must be implement NavigationAction.")
        }

        minWidth = requireContext().resources.getDimension(R.dimen.nav_item_min_width).toInt()
        maxWidth = requireContext().resources.getDimension(R.dimen.nav_item_width).toInt()

        internalOffset = (maxWidth - minWidth) / 10
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentNavBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.root.children.forEach {
            it.setOnKeyListener(this)
            if (it is HeaderView) {
                it.addOnSelectChangeListener(this)
                it.setOnClickListener { view ->
                    actionListener.onClicked(view.tag as String)
                }
            }
        }

        viewModel = ViewModelProvider(requireActivity())[MainViewModel::class.java].apply {
            selectedType.observe(viewLifecycleOwner) { tag ->
                Log.d(MainActivity.TAG, "selectedType: $tag")
                binding.root.children.forEach {
                    if (it.tag == tag) {
                        it.requestFocus()
                    }
                }
            }
        }

    }

    override fun onKey(view: View?, keyCode: Int, keyEvent: KeyEvent): Boolean {
        if (KeyEvent.ACTION_DOWN == keyEvent.action) {
            if (keyCode ==  KeyEvent.KEYCODE_DPAD_RIGHT) {
                closeNav(true)
                view?.isSelected = true
                actionListener.onStateChanged(false)
                return true
            }
        }

        return false
    }

    override fun onStop() {
        super.onStop()
        with(binding.root) {
            handler.removeCallbacks(minWidthRunnable)
            handler.removeCallbacks(maxWidthRunnable)
            binding.root.layoutParams.width = if (isOpen) maxWidth else minWidth
        }
    }

    override fun onSelectChanged(view: View) {
        val tag = view.tag as String
        if (isOpen && viewModel.selectedType.value != tag) {
            viewModel.selectedType.postValue(tag)
        }
    }


    fun closeNav(showAnimation: Boolean) {
        isOpen = false
        with(binding.root) {
            children.map { it as HeaderView }.forEach {
                it.isFocusable = false
                it.close()
                it.clearFocus()
            }

            if (showAnimation) {
                layoutParams.width = max(minWidth, (measuredWidth - internalOffset * 3))
                requestLayout()

                handler.removeCallbacks(minWidthRunnable)
                handler.removeCallbacks(maxWidthRunnable)
                handler.post(minWidthRunnable)
            }
        }
    }

    fun openNav(showAnimation: Boolean) {
        isOpen = true
        with(binding.root) {
            children.map { it as HeaderView }.forEach {
                it.isFocusable = true
                if (it.tag == viewModel.selectedType.value) {
                    it.requestFocus()
                }
                it.open()
            }

            if (showAnimation) {
                layoutParams.width = max(minWidth, (measuredWidth + internalOffset * 3))
                requestLayout()

                handler.removeCallbacks(minWidthRunnable)
                handler.removeCallbacks(maxWidthRunnable)
                handler.post(maxWidthRunnable)
            }
        }
    }


    companion object {
        const val TAG = "NavFragment"
    }
}

interface NavigationAction {

    fun onStateChanged(expanded: Boolean)

    fun onClicked(tag: String?)
}