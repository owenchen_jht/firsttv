package com.owen.firsttv.search

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.owen.firsttv.R

class SearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
    }


    companion object {

        fun launch(context: Context) {
            context.startActivity(Intent(context, SearchActivity::class.java))
        }

    }
}