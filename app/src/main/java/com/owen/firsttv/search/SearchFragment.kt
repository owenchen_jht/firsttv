package com.owen.firsttv.search


import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.leanback.app.SearchSupportFragment
import androidx.leanback.widget.ArrayObjectAdapter
import androidx.leanback.widget.ListRowPresenter
import androidx.leanback.widget.ObjectAdapter
import com.owen.firsttv.main.ExchangeFocusListener

class SearchFragment: SearchSupportFragment(),
    SearchSupportFragment.SearchResultProvider,
    ExchangeFocusListener
{

    private lateinit var rowsAdapter : ArrayObjectAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        rowsAdapter = ArrayObjectAdapter(ListRowPresenter())
        setSearchResultProvider(this)
    }

    override fun getResultsAdapter(): ObjectAdapter = rowsAdapter

    override fun onQueryTextChange(newQuery: String?): Boolean {
        Log.d(TAG, "onQueryTextChange: $newQuery")
        Toast.makeText(requireContext(), "onQueryTextChange newQuery: $newQuery", Toast.LENGTH_SHORT).show()

        return false
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        Log.d(TAG, "onQueryTextSubmit: $query")
        Toast.makeText(requireContext(), "onQueryTextSubmit query: $query", Toast.LENGTH_SHORT).show()

        return false
    }

    override fun requireFocus() {
    }

    override fun shouldOpenNav(): Boolean {
        return true
    }


    companion object {

        const val TAG = "SearchFragment"
    }
}